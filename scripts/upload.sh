#!/bin/sh

quiet() {
    "$@" >/dev/null 2>&1
}

user='pi'
machine='127.0.0.1'
dir='Travelbox'

while [ ! -z "$1" ]
do
    if [ "$1" = "-u" ]
    then
        shift
        user="$1"
    elif [ "$1" = "-m" ]
    then
        shift
        machine="$1"
    else
        dir="$1"
    fi
    shift
done

echo "Copying files as '$user' to '~/$dir' on '$machine'."

echo "Copying script files."
rsync applications/* lib/* services/* "$user@$machine:~/$dir"
echo "Copying configuration to $dir/etc. You have to copy to /etc."
rsync -r config/* "$user@$machine:~/$dir/etc"

