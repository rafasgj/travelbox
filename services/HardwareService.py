#!/usr/bin/env python

from Configuration import *
import ipc

import LCDManager
import LED
import ButtonManager

configuration = Configuration()

port = int(configuration.read('hwsvc_port', 15234))
server = ipc.server(port=port)
server.registerObject("travelbox.lcd", LCDManager.LCDManager())

error_led = map(int, configuration.read_array('error_led'))
info_led  = map(int, configuration.read_array('info_led'))
server.registerObject("travelbox.errorled", LED.LED(error_led))
server.registerObject("travelbox.infoled", LED.LED(info_led))
server.registerObject("travelbox.buttons", ButtonManager.ButtonManager())

server.start()
