#!/usr/bin/env python

from Application import Application
import Menu
from ImportPhotos import *
from PlayMovies import *
from Monitors import *
from Slideshow import *
from Shutdown import *

class TravelBox(Application):
	def __init__(self):
		Application.__init__(self,"Travel Box")
		self.menu = Menu.Menu(self, [
			("Import Photos", ImportPhotos()),
			("Slideshow", Slideshow()),
			("Play Movies", PlayMovies()),
			("Monitors", Monitors()),
			("Shutdown", Shutdown())
		])

	def run(self):
		self.acquireLCD()
		self.menu.loop()
		self.releaseLCD()

if __name__ == "__main__":
	app = TravelBox()
	app.run()

