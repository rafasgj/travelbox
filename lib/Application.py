#!/usr/bin/env python

from Configuration import *
import ipc

class Application:

    OK = 1
    CANCEL = 0

    def __init__(self, name):
        self.name = name
        self.menu = None
        port = int(Configuration().read('hwsvc_port'))
        self.__lcd = ipc.proxy("travelbox.lcd",port)
        self.buttons = ipc.proxy("travelbox.buttons",port)

    def app_exited(self, app_name, app_object):
        self.acquireLCD()

    def run(self):
        raise NotImplemented("Method run() must be implemented")

    def acquireLCD(self):
        self.__lcd.acquire(self.name)

    def releaseLCD(self):
        self.__lcd.release()

    def writeLCD(self, text, line):
        self.__lcd.write(self.name, text, line)

    def write(self, *data):
        for i in xrange(len(data)):
            self.writeLCD(data[i],i + 1)

    def confirmation(self, caption):
        self.write(caption,"\x7F OK    Cancel \x7E")
        while (True):
            button = self.buttons.wait_for_click()
            if button == self.buttons.ok():
                return Application.OK
            elif button == self.buttons.cancel():
                return Application.CANCEL

