#/usr/bin/env python

import threading, Queue, time
import Configuration

import gpio
    
class LED:
    def __init__(self, name, pins):
        self.name = name
        self.pins = pins
        self.value = False
        self.queue = Queue.Queue()
        self.thread = threading.Thread(target=self.led_thread)
        self.thread.daemon = True
        self.thread.start()

    def blink(self, frequency, time = 1):
        on_time = 1.0 / (1.0 * frequency * 2)
        for i in xrange(0,int(frequency * time * 2), 2):
            threading.Timer(on_time * i, self.on,()).start()
            threading.Timer(on_time * (i+1), self.off,()).start()

    def on(self, timeout = -1):
        self.state(True, timeout)

    def off(self, timeout = -1):
        self.state(False, timeout)

    def state(self, high, timeout = -1):
        self.queue.put((high, timeout))

    def led_thread(self):
        while True:
            try:
                self.value, timeout = self.queue.get()
                # schedule led "flip"
                if (timeout > 0):
                    threading.Timer(timeout, self.state,((True,0))).start()
            except Queue.Empty:
                pass
            

class LEDManager:
    @staticmethod
    def __read_bits_config():
        bits_cfg = LEDManager.config.read('led_bits','17/27/22')
        bit_pins = bits_cfg.split('/')
        if len(bit_pins) != 3:
            bit_pins = bits_cfg.split(':')
        if len(bit_pins) != 3:
            bit_pins = bits_cfg.split(',')
        if len(bit_pins) != 3:
            return None
        return map(int,bit_pins)

    ON = True
    OFF = False

    RED    = 0x01
    GREEN  = 0x02
    BLUE   = 0x04
    YELLOW = 0x08
    PHOTO  = 0x10
    VIDEO  = 0x20
    PROBLEM = 0x30

    __leds = [
        LED("Red", [0]),
        LED("Green", [1]),
        LED("Blue", [0,1]),
        LED("Yellow", [2]),
        LED("Photo", [0,1]),
        LED("Video", [0,2])
    ]

    config = Configuration.Configuration()
    __bits = []

    lock = threading.Lock()

    @staticmethod
    def config():
        LEDManager.config = Configuration.Configuration()
        if len(LEDManager.__bits) == 0:
            LEDManager.__bits = LEDManager.__read_bits_config()

    def __init__(self):
        LEDManager.config()
        self.pins = [
            gpio.Pin(LEDManager.__bits[0], gpio.Pin.OUT),
            gpio.Pin(LEDManager.__bits[1], gpio.Pin.OUT),
            gpio.Pin(LEDManager.__bits[2], gpio.Pin.OUT)
        ]
        self.thread = threading.Thread(target=self.led_thread)
        self.thread.daemon = True
        self.thread.start()

    def __getPin(self, leds):
        pins = []
        for bit in xrange(len(LEDManager.__leds)):
            if leds & (1 << bit):
                pins.append(bit)
        return pins

    def state(self, leds, value, timeout = -1):
        pins = self.__getPin(leds)
        for pin in pins:
            if value:
                self.__leds[pin].on(timeout)
            else:
                self.__leds[pin].off()

    def blink(self, leds, frequency, time = 1):
        pins = self.__getPin(leds)
        for pin in pins:
            self.__leds[pin].blink(frequency, time)

    def led_thread(self):
        while True:
            for led in self.__leds:
                for pin in self.pins:
                    pin.set_state(False)
                for pin in led.pins:
                    self.pins[pin].set_state(led.value)

    def __show_message(self, *args):
        LEDManager.lock.acquire()
        print " ".join(map(str,args))
        LEDManager.lock.release()


if __name__ == "__main__":
    man = LEDManager()
    #man.state(LEDManager.RED, LEDManager.ON)
    #man.state(LEDManager.RED | LEDManager.PROBLEM, LEDManager.ON, 2)
    man.blink(LEDManager.GREEN, 2, 6)
    
    import time
    time.sleep(10)

    man.state(LEDManager.RED, LEDManager.OFF)
