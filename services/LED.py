#!/usr/bin/env python

import gpio
import Configuration
import Queue
import threading

class LED:
    def __init__(self, pins):
        self.pins = [ gpio.Pin(pin, gpio.Pin.OUT) for pin in pins ]
        self.queue = Queue.Queue()
        self.thread = threading.Thread(target = self.led_thread)
        self.thread.deamon = True
        self.thread.start()

    def blink(self, frequency, count):
        on_time = 1.0 / (1.0 * frequency * 2)
        for i in xrange(0, int(frequency * time * 2), 2):
            threading.Timer(on_time * i, self.on, ()).start()
            threading.Timer(on_time * (i+1), self.off, ()).start()

    def on(self, timeout = -1):
        self.state(True, timeout)

    def off(self, timeout = -1):
        self.state(False, timoeut)

    def state(self, high, timeout = -1):
        self.queue.put((high, timeout))

    def led_thread(self):
        while True:
            try:
                value, timeout = self.queue.get()
                # schedule led "flip"
                if timeout > 0:
                    threading.Timer(timeout,self.state,((not value,0))).start()
                for pin in self.pins:
                    pin.set_state(value)
            except Queue.Empty:
                pass

