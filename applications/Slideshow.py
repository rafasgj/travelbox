#!/usr/bin/env python

from Application import Application
import Menu
import time
import os
import threading, subprocess
from random import shuffle
from functools import partial

def flatten_list(data):
    return [ item for sublist in data for item in sublist ]

def show_images(filelist):
    #fim -a -q -A -w -c 
    options = [
        '/usr/bin/fim',
        '-d','/dev/fb0',
        '-T','1', '-q',
        '-c',
        "_scale_style 'w'; while(1){ scale '30%'; next; reload; display; sleep '1'}"]
    map(options.append, filelist)
    subprocess.call(options)

class SlideshowBase(Application):
    def __init__(self, name, mpoint):
        Application.__init__(self, name)
        fs = []
        for directory, dirs, filelist in os.walk(mpoint):
            fs.append([os.path.join(directory,f) for f in filelist
                          if not f.startswith('.') and f.endswith('.jpg')])
        self.files = flatten_list(fs)
        
    def start_slideshow(self, data):
        t = threading.Thread(target=show_images,args=([data]))
        t.daemon = True
        t.start()

class SlideshowRandom(SlideshowBase):
    def __init__(self, mpoint):
        SlideshowBase.__init__(self,"Random Slideshow",mpoint)

    def run(self):
        shuffle(self.files) 
        self.start_slideshow(self.files)

class SlideshowByDate(SlideshowBase):
    def __init__(self,mpoint):
        SlideshowBase.__init__(self,"Date Slideshow",mpoint)
        ini = len(mpoint.split('/'))
        dates = set()
        for f in self.files:
            dates.add("/".join(f.split('/')[ini:ini+3]))
        self.dates = list(dates)
        self.dates.sort()

    def create_filters(self):
        result = []
        for date in self.dates:
            images = [f for f in self.files if f.find(date) > 0]
            bound = partial(self.start_slideshow, images)
            result.append((date,bound))
        return result

    def run(self):
        self.acquireLCD()
        self.write("Playing slideshow by date.")
        apps = self.create_filters()
        apps.append(('Cancel',Menu.Exit()))
        menu = Menu.Menu(self, apps)
        menu.loop()
        self.releaseLCD()

class Slideshow(Application):
    def __find_catalog(self):
        self.write("Find files...")
        mnt = os.listdir("/Volumes")
        found = []
        for m in mnt:
            dcim = "/Volumes/{}/TravelBox".format(m)
            if os.path.isdir(dcim):
                found.append(m)
        for m in found:
            self.write("Found catalog on", m)
            time.sleep(2)
            if self.confirmation("Use: {}".format(m)) == Application.OK:
                return m
        self.write("No catalog found")
        time.sleep(3)
        return None

    def __init__(self):
        Application.__init__(self,"Slideshow")
        self.acquireLCD()
        mpoint = "/Volumes/{}/TravelBox".format(self.__find_catalog())
        self.releaseLCD()
        self.menu = Menu.Menu(self, [
            ("By Date",SlideshowByDate(mpoint)),
            ("Random", SlideshowRandom(mpoint)),
            ("Cancel", Menu.Exit())
        ])
    
    def run(self):
        self.acquireLCD()
        self.menu.loop()
        self.releaseLCD()

if __name__ == "__main__":
    show = Slideshow()
    show.run()


