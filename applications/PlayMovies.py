#!/usr/bin/env python

import Application, Menu
import sys, os
import mimetypes, re
import subprocess

class Movie(Application.Application):
    def __init__(self, name, movieFile):
        Application.Application.__init__(self,"Movie")
        self.name = name
        self.movieFile = movieFile

    def run(self):
        self.acquireLCD()
        self.write("Playing Movie...",self.name)
        self.releaseLCD()
        try:
            # play movie with omxplayer
            player=['omxplayer',
                    '-o','hdmi',
                    '-b',
                    '--no-osd',
                    '--no-ghost-box']
            player.append(self.movieFile)
            process = subprocess.Popen(player, stdin=subprocess.PIPE)
            process.stdin.write('.')
            process.wait()
        except:
            # TODO Don't hide every exception.
            pass

class PlayMovies(Application.Application):
    def __init__(self):
        Application.Application.__init__(self,"Play Movies")
    
    def run(self):
        self.acquireLCD()
        entries = self.readFilmList()
        entries.append(("Cancel",Menu.Exit()))
        menu = Menu.Menu(self, entries)
        menu.loop()
        self.releaseLCD()

    def isMovie(self, filename):
        # It does not work well with m4v movies.
        fileType = mimetypes.guess_type(filename)[0]
        if fileType == None:
            return False
        else:
            return re.match('video/',fileType)

    def movie_name(self, filename):
        return os.path.splitext(os.path.basename(filename))[0]

    def readFilmList(self):
        d = os.path.join(os.environ['HOME'],"Movies")
        result=self.readMoviesFromDir(d)
        for directory in os.listdir("/Volumes"):
            d = os.path.join(os.path.join("/Volumes",directory), "Movies")
            result.extend(self.readMoviesFromDir(d))
        entries = [ (self.movie_name(f),Movie(self.movie_name(f),f))
                    for f in result ]
        return entries

    def readMoviesFromDir(self, movieDir):
        if not os.path.isdir(movieDir): return []
        movieFiles = [ os.path.join(movieDir,f)
                       for f in os.listdir(movieDir)
                       if os.path.isfile(os.path.join(movieDir,f)) ]
                        #for f in movieFiles if self.isMovie(f) ]
        return movieFiles

if __name__ == "__main__":
    PlayMovies().run()

