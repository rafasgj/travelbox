#!/usr/bin/env python

try:
    import RPi.GPIO as gpio
    gpio.setwarnings(False)
except:
    import random
    import threading
    class gpio:
        IN = True
        OUT = False
        PUD_DOWN = 1
        BCM = 1
        RISING = 1
        FALLING = 2
        pins = [ None ] * 30
        state = [ False ] * 30
        @staticmethod
        def setmode(*args, **kargs): pass
        @staticmethod
        def cleanup(*args, **kargs): pass
        @staticmethod
        def setup(*args, **kargs): pass
        @staticmethod
        def input(pin, *args, **kargs):
            retval = gpio.state[pin]
            gpio.state[pin] = False
            return retval
        @staticmethod
        def output(*args, **kargs):
            pin, state = args
        @staticmethod
        def add_event_detect(pin, evt, *args,**kargs):
            try: p = gpio.pins[pin]
            except: p = None
            if p:
                p.append(kargs['callback'])
            else:
                gpio.pins[pin] = [ evt, kargs['callback'] ]
    
    def input_thread():
        while True:
            pinid = int(raw_input(''))
            try:
                pin = gpio.pins[pinid]
            except:
                pin = None
                continue
            if pin:
                evt, cbs = pin[0], pin[1:]
                gpio.state[pinid] = True
                for cb in cbs:
                    cb(pinid)

    thread = threading.Thread(target=input_thread)
    thread.daemon = True
    thread.start()

class Pin:
    IN = gpio.IN
    OUT = gpio.OUT

    def __init__(self, pin, direction):
        gpio.setmode(gpio.BCM)
        self.direction = direction
        self.pin = pin
        gpio.setup(pin, direction, pull_up_down=gpio.PUD_DOWN)
    
    def __del__(self):
        gpio.cleanup()

    def set_rise_callback(self, cb, bnc=0):
        if self.direction != gpio.IN: return
        gpio.add_event_detect(self.pin, gpio.RISING, callback=cb, bouncetime=bnc)

    def set_fall_callback(self, cb, bnc=0):
        if self.direction != gpio.IN: return
        gpio.add_event_detect(self.pin, gpio.FALLING, callback=cb, bouncetime=bnc)

    def get_state(self):
        return gpio.input(self.pin)

    def set_state(self, state):
        if self.direction != gpio.OUT: return
        gpio.output(self.pin, state)

    def flip(self):
        self.set_state(not self.get_state())

if __name__ == "__main__":
    import time
    def cb_test():
        print "Pressed button"

    p = Pin(4,Pin.IN)
    p.set_fall_callback(cb_test)
    time.sleep(10)

