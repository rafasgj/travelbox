#!/bin/sh (source'd)

die() {
    echo "$*"
    return 1
}

base="`pwd`"

[ -f "$base/set_env.sh" ] && base="`dirname "$base"`"
[ -f "$base/scripts/set_env.sh" ] || die "Could not found Travelbox sources"

directories=('applications' 'services' 'lib')
for dir in ${directories[@]}
do
    PYTHONPATH=$PYTHONPATH:"$base/$dir"
done

export PYTHONPATH
