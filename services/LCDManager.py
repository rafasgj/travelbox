#!/usr/bin/env python

try: from lcd import lcddriver
except:
    print "Could not load LCD module. Using 'console' as fallback."
    class lcddriver:
        @staticmethod
        def lcd(*args, **kargs): pass


import gpio

import ipc, Configuration

import time, threading
from Queue import *

#debug
import traceback

class LCDManager:
    def __config__(self):
        config = Configuration.Configuration()
        self.lcd_enable = config.read_int('lcd_enable',4)
        self.lcd_length = config.read_int('lcd_length', 16)
        self.lcd_lines = config.read_int('lcd_lines', 2)
        self.mon_sleep = config.read_int('lcd_sleep',60)

    def __init_lcd__(self):
        self.pin = gpio.Pin(self.lcd_enable, gpio.Pin.OUT)
        self.text = [''] * self.lcd_lines
        self.lcd_on = False
        try:
            self.lcd = lcddriver.lcd(self.lcd_lines, self.lcd_length)
            self.wakeup()
        except Exception:
            traceback.print_exc()
            print "Failed to create LCD object. Using 'console' as fallback"
            self.lcd = None

    def __init__(self):
        self.__config__()
        self.__init_lcd__()
        self.timeout = 0
        self.application = None
        # Thread data
        self.q = Queue()
        self.cv = threading.Condition()
        self.thread = threading.Thread(target=self.__backgroundWrite)
        self.thread.daemon = True   
        self.thread.start()
        # sleeper thread
        self.lcd_sleep = threading.Thread(target=self.__lcdWatchdog)
        self.lcd_sleep.daemon = False
        self.lcd_sleep.start()

    def __power_lcd(self, state):
        old = self.lcd_on
        self.lcd_on = state
        try:
            self.pin.set_state(self.lcd_on)
            if self.lcd_on:
                self.lcd = lcddriver.lcd(self.lcd_lines, self.lcd_length)
        except:
            # get back to previous state on error.
            self.__power_lcd(not state)

    def __send_to_device(self, text, line):
        self.timeout = 0
        if not self.lcd_on:
            self.__power_lcd(True)
        if self.lcd:
            self.lcd.write_line(text,line)
        else:
            print "{}: {}".format(str(line),text)

    def __write_text(self):
        for line in xrange(self.lcd_lines):
            self.__send_to_device(self.text[line], line + 1)

    def __backgroundWrite(self):
        old = [''] * self.lcd_lines
        elem = None
        while True:
            # Wait until some item is available.
            self.cv.acquire()
            self.timeout = 0
            while self.q.empty():
                #
                # TODO: if enough time has elapsed, turn LCD OFF.
                #
                # wait for half a second
                self.cv.wait(0.5)
            text, line = self.q.get()
            idx = line - 1
            self.cv.release()
            if self.text[idx] != text:
                self.text[idx] = text
            self.__write_text()

    def __lcdWatchdog(self):
        while True:
            if self.timeout >= self.mon_sleep:
                self.sleep()
            self.timeout += 1
            time.sleep(1)

    def clear(self):
        self.cv.acquire()
        for i in xrange(self.lcd_lines):
            self.q.put(('',i+1))
        self.cv.notify()
        self.cv.release()

    def acquire(self, application):
        if not self.lcd_on:
            self.wakeup()
        if self.application != application:
            self.application = application
            self.clear()
            self.write(application, application, 1)

    def release(self):
        self.application = None

    def write(self, application, text, line):
        if self.application == None:
            self.acquire(application)
        if application != self.application:
            return
        if line < 1 or line > self.lcd_lines:
            return
        self.cv.acquire()
        self.q.put((text, line))
        self.cv.notify()
        self.cv.release()

    def wakeup(self):
        if not self.lcd_on:
            self.__power_lcd(True)
            self.__write_text()

    def sleep(self):
        if self.lcd_on:
            self.__power_lcd(False)
            if not self.lcd:
                print "POWER OFF"

