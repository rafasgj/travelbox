#!/usr/bin/env python

from Application import Application

import os

class Shutdown(Application):
    def __init__(self):
        Application.__init__(self,"Shutdown")
    
    def run(self):
        self.acquireLCD()
        if self.confirmation("Shutdow now?") == Application.OK:
            self.write("Shuting down system.")
            os.system("/sbin/poweroff")
        self.releaseLCD()

