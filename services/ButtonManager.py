#!/usr/bin/env python

import gpio
import Configuration

from threading import Condition

class InputButton:
    def __init__(self, pin, onrelease = None, onpress = None):
        self.pin = gpio.Pin(pin, gpio.Pin.IN)
        if onrelease != None:
            self.pin.set_fall_callback(self.on_released)
        elif onpress != None:
            self.pin.set_rise_callback(self.on_pressed)
        self.onpress = onpress
        self.onrelease = onrelease
    
    def state(self):
        return self.pin.get_state()

    def on_pressed(self, pin):
        if self.onpress:
            self.onpress(pin)

    def on_released(self, pin):
        if self.onrelease:
            self.onrelease(pin)

class ButtonManager:

    OK = 0
    CANCEL = 2
    PREV = 0
    NEXT = 2
    SELECT = 1

    def __init__(self):
        self.last_pin = -1
        config = Configuration.Configuration()
        pins = map(int,config.read_array('buttons',[22,23,24]))
        self.buttons = [ InputButton(pin, onrelease=self.button_pressed)
                         for pin in pins ]
        self.lock = Condition()

    def button_pressed(self, pin):
        self.lock.acquire()
        self.last_pin = pin
        self.lock.notify()
        self.lock.release()

    def state(self):
        return [ pin.state() for pin in self.buttons ]

    def wait_for_click(self):
        result = -1
        self.lock.acquire()
        self.lock.wait()
        for i in xrange(len(self.buttons)):
            if self.buttons[i].pin.pin == self.last_pin:
                result = i
                break
        self.last_pin = -1
        self.lock.release()
        return result

    def ok(self):
        return ButtonManager.OK
    
    def cancel(self):
        return ButtonManager.CANCEL

    def next(self):
        return ButtonManager.NEXT

    def previous(self):
        return ButtonManager.PREV

    def select(self):
        return ButtonManager.SELECT


if __name__ == "__main__":
    import time, sys
    b1 = InputButton(4, lambda pin: sys.stdout.write(str(pin)+"\n"))
    b2 = InputButton(27, lambda pin: sys.stdout.write(str(pin)+"\n"))
    print "Testing buttons on pins 4 and 27"
    print "Available pins: 4 27"
    print "Available time: 10s"
    time.sleep(10)
    print "Testing ButtonManager (with pins configured for Travelbox)"
    bm = ButtonManager()
    print "Next button:",bm.wait_for_click()

