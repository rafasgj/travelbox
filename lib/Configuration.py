#!/usr/bin/env python

SYSTEM_CONFIGURATION = '/etc/Travelbox/config'

import traceback

class Configuration:

    default = {
        "hwsvc_port": 51234
    }

    def __init__(self):
        self.configuration = Configuration.default
        try:
            f = open(SYSTEM_CONFIGURATION,'r')
            for line in f.xreadlines():
                line = line.strip()
                # ignore comments
                if len(line) == 0 or line[0] == "#": continue
                # split line in a key-value pair.
                k,v = line.split('=', 1)
                self.configuration[k.strip()] = v.strip()
        except:
            pass

    def read_int(self,id, default=None):
        try: return int(self.configuration[id])
        except: return int(default)

    def read(self,id, default=None):
        try: return self.configuration[id]
        except: return default

    def read_array(self, id, default=[]):
        data = self.read(id)
        if not data: return default
        array = data.split(',')
        if len(array) == 1: array = data.split(':')
        if len(array) == 1: array = data.split('/')
        return array

if __name__ == "__main__":
    config = Configuration()
    print config.configuration
