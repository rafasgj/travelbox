#!/usr/bin/env python

from Application import Application

class @NAME@(Application):
	def __init__(self):
		Application.__init__(self,"@TITLE@")
	
	def run(self):
		raise NotImplemented("Method run() not implemented in @NAME@")

if __name__ == '__main__':
    @NAME@().run()

