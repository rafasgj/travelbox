#!/bin/sh

dir="`dirname "$0"`"

if [ -f "set_env.sh" ]
then
    pushd .. 2>&1 >/dev/null
else
    pushd . 2>&1 >/dev/null
fi

. scripts/set_env.sh
python services/HardwareService.py

popd 2>&1 >/dev/null

