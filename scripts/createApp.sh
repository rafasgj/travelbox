#!/bin/sh

if [ $# -ne 2 ]
then
	echo "Usage:\n\t`basename $0` App_Class App_Name"
	exit
fi

name="$1"
desc="$2"

dir=`dirname $0`
outdir="${dir}/../Applications/"

sed -e "s/@NAME@/$name/g
s/@TITLE@/$desc/g" < "$dir/app_model.py" > "$outdir/$name.py"

