#!/usr/bin/env python

import time
import ipc
import Configuration
from Application import Application

class Exit:
    pass

class Menu:
    def __init__(self, application, options):
        self.options = options
        self.application = application
        port = int(Configuration.Configuration().read('hwsvc_port'))
        self.button_svc = ipc.proxy("travelbox.buttons",port)

    def loop(self):
        count = 0;
        total = len(self.options)

        current, app = self.options[count]
        self.application.write(
                self.application.name,
                "\x7F{0:^14}\x7E".format(current[:14]))
        
        while True:
            old = count
            time.sleep(0.5)
            button = self.button_svc.wait_for_click()
            if button == self.button_svc.select():
                current, app = self.options[count]
                if isinstance(app, Exit):
                    return
                elif isinstance(app, Application):
                    app.run()
                    if self.application:
                        self.application.app_exited(current, app)
                else:
                    app()
                self.application.write(
                        self.application.name,
                        "\x7F{0:^14}\x7E".format(current[:14]))
            if button == self.button_svc.next():
                count += 1
            if button == self.button_svc.previous():
                count -= 1
            if count < 0:
                count = total - 1
            count = count % len(self.options)
            if count != old:
                current, app = self.options[count]
                self.application.write(
                        self.application.name,
                        "\x7F{0:^14}\x7E".format(current[:14]))
                                

if __name__ == "__main__":
    import Application
    class A(Application):
        def __init__(self):
            Application.__init__(self,'A')
        def run(self):
            print "A running"
            self.write("A running.","")
    
    class B(Application):
        def __init__(self):
            Application.__init__(self,'B')
        def run(self):
            print "B running"
            self.write(self,"B running.","")
            
    class Test(Application):
        def __init__(self):
            Application.__init__(self,'Test')
            self.menu = Menu(self, [('App A',A()),('App B',B())])
        def run(self):
            self.acquireLCD()
            self.menu.loop()
            self.releaseLCD()
    Test().run()
