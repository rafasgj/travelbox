#!/usr/bin/env python


try: import pyexiv2
except:
    class PyExiv2Image:
        def __init__(self):
            self.dimensions = (1024,768)
            self.mime_type = 'image/jpeg'
        def write_to_file(*args, **kargs): pass
    class ImageMetadata():
        def __init__(self):
            self.previews = [ PyExiv2Image() ]
    class pyexiv2:
        @staticmethod
        def ImageMetadata(*args,**kargs):
            return ImageMetadata()

from Application import *
import Menu

import sys
import subprocess
import os, shutil
from time import strftime, gmtime, sleep

class ImportPhotos(Application):
    CATALOG = ".photo_catalog"
    def __init__(self):
        Application.__init__(self,"Import Photos")
        self.menu = None
        self.catalog = set()

    def load_catalog(self, catalog_dir):
        catalog = "{}/{}".format(catalog_dir, ImportPhotos.CATALOG)
        f = None
        try:
            f = open(catalog,"r")
            self.catalog.update(map(str.strip,f.readlines()))
            f.close()
        except:
            if f: f.close()
            pass

    def find_catalog(self):
        self.write("Find files...")
        mnt = os.listdir("/Volumes")
        found = []
        notfound = []
        for m in mnt:
            mpoint = "/Volumes/{}".format(m)
            dcim = "{}/TravelBox".format(mpoint,m)
            if os.path.isfile("{}/{}".format(dcim, ImportPhotos.CATALOG)):
                found.append(m)
            elif os.path.isdir(mpoint):
                    notfound.append(m)
        for m in found:
            self.write("Found catalog on", m)
            sleep(2)
            if self.confirmation("Use: {}".format(m)) == Application.OK:
                return m
        for m in notfound:
            if self.confirmation("Use: {}".format(m)) == Application.OK:
                return m
        return None

    def find_files(self):
        self.write("Find files...")
        mnt = os.listdir("/Volumes")
        files = []
        for m in mnt:
            dcim = "/Volumes/{}/DCIM".format(m)
            if os.path.isdir(dcim):
                for directory, dirs, filelist in os.walk(dcim):
                    files.append([os.path.join(directory,f)
                                        for f in filelist])
        # This flattens the list of lists. Awful, but works fine.
        return [ f for sublist in files for f in sublist ]

    def create_dirs(self, targetdir):
        target = ''
        for d in targetdir.split("/"):
            target = "/".join(['',target,d])
            if not os.path.exists(target):
                os.mkdir(target)

    def catalog_metadata(self, filename, catalog_path):
        fstat = os.stat(filename)
        # Create target path
        parts = filename.split('/')
        i = 0
        fname = parts[-1]
        datestr = strftime("%Y/%m/%d",gmtime(fstat[-1]))
        targetdir = "{}/{}".format(catalog_path, datestr)
        targetpath = "{}/{}".format(targetdir,fname)
        
        camdir = ''
        while i < len(parts) and parts[i] != 'DCIM': i += 1
        else: camdir = "/".join(parts[i+1:])
        catalogentry = "/".join([datestr,camdir])
        return (fname, datestr, targetdir, targetpath, catalogentry)

    def import_files(self, catalog_path, filelist):
        total = len(filelist)
        self.write("Photos found:","{}".format(total))
        sleep(2)
        count = 0
        imported = 0
        for f in filelist:
            count += 1
            fname, datestr, targetdir, targetpath, catalogentry = \
                self.catalog_metadata(f, catalog_path)
            if not catalogentry in self.catalog:
                self.write("Importing",fname)
                # ensure directory exists
                if not os.path.exists(targetdir):
                    os.makedirs(targetdir)
                    #self.create_dirs(targetdir)
                elif not os.path.isdir(targetdir):
                    self.write("Failed Directory", targetdir)
                    continue
                # copy file
                self.write("Copying...", "{} of {}".format(count,total))
                try:
                    # Create unique name
                    tgt = targetpath
                    i = 1
                    while os.path.isfile(tgt):
                        fext = os.path.splitext(targetpath)
                        tgt = "{f[0]}-{i}{f[1]}".format(i=i,f=fext)
                        i += 1
                    # do copy
                    shutil.copy2(f, tgt)
                    # add to catalog
                    self.catalog.add(catalogentry)
                    # done message
                    self.write("Copied", fname)
                    imported += 1
                    # extract JPEG image
                    fext = os.path.splitext(tgt)
                    try:
                        exif = pyexiv2.ImageMetadata(tgt)
                        exif.read()
                        if len(exif.previews) > 0:
                            img = exif.previews[-1]
                            img.write_to_file("{}".format(fext[0]))
                    except: 
                        self.write('EXIV Error')
                except:
                    self.write("Failed copy of", fname)
        self.write("Total imported", "{} photos".format(imported))
        sleep(3)

    def update_catalog(self, catalog_path):
        catalog_file = "{}/{}".format(catalog_path, ImportPhotos.CATALOG)
        try:
            f = open(catalog_file,"w")
            for img in self.catalog:
                f.write("{}\n".format(img))
            f.close()
        except:
            self.write("Failed to create","catalog.")
            raise

    def umount_volumes(self, catalog_volume):
        mnt = os.listdir("/Volumes")
        for m in mnt:
            if m == catalog_volume: continue
            self.write("Ejecting",m)
            mpoint = "/Volumes/{}".format(m)
            subprocess.call(["/bin/umount",mpoint])
            os.rmdir(mpoint)

    def run(self):
        self.acquireLCD()
        catalog_volume = self.find_catalog()
        catalog_dir = "/Volumes/{}/TravelBox".format(catalog_volume)
        if not catalog_dir:
            self.write("No catalog found.", "Aborting.")
            sleep(3)
            self.releaseLCD()
            return
        self.load_catalog(catalog_dir)
        self.import_files(catalog_dir, self.find_files() )
        self.update_catalog(catalog_dir)
        self.umount_volumes(catalog_volume)
        self.releaseLCD()

if __name__ == "__main__":
    ImportPhotos().run()

