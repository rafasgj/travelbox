#!/usr/bin/env python

import Application
import threading
import time

import array, socket, struct, fcntl

def getNetInfo(iface = "wlan0"):
    "Get network IP and ESSID."
    # create socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sockfd = sock.fileno()
    # ioctl ids
    SIOCGIFADDR = 0x8915
    SIOCGIWESSID = 0x8B1B
    # get IP
    ifreq = struct.pack('16sH14s', iface, socket.AF_INET, '\x00'*14)
    try: res = fcntl.ioctl(sockfd, SIOCGIFADDR, ifreq)
    except: return None
    ip = struct.unpack('16sH2x4s8x', res)[2]
    # get ssid
    essid = array.array("c", "\0" * 32)
    essidPointer, essidLength = essid.buffer_info()
    request = array.array("c",
                          iface.ljust(16, "\0") +
                          struct.pack("PHH",essidPointer,essidLength,0)
                         )
    fcntl.ioctl(sock.fileno(), SIOCGIWESSID, request)
    # pack result
    return [socket.inet_ntoa(ip), essid.tostring().rstrip("\0")]

class Monitors(Application.Application):
    def __init__(self):
        Application.Application.__init__(self,"System Monitors")
        self.data = self.read_procstat()
        self.cpu = 0
        self.running = False

    def read_procstat(self):
        try:
            f = open('/proc/stat')
            if f:
                for data in f.readlines():
                    items = data.split()
                    if items[0] == 'cpu':
                        work_jiffies = 0
                        total_jiffies = 0
                        for i in xrange(1,len(items)):
                            total_jiffies += int(items[i])
                            if i < 4:
                                work_jiffies += int(items[i])
                f.close()
            return work_jiffies, total_jiffies
        except: pass
        return 0,0

    def compute_cpu(self):
        data = self.read_procstat()
        work_diff = data[0] - self.data[0]
        total_diff = data[1] - self.data[1]
        if (total_diff != 0):
            self.cpu = work_diff / (1.0 * total_diff)
        self.data = data[:]
   
    def search_ip(self):
        ifaces = [ 'wlan', 'eth', 'en', 'ppp' ]
        for iface in ifaces:
            for num in xrange(4):
                ifc = "{}{}".format(iface,num)
                netinfo = getNetInfo(ifc)
                if netinfo:
                    ip,ssid = netinfo
                    self.write(ssid, ip)
                    time.sleep(3)

    def do_cpu(self):
        self.compute_cpu()
        self.write("CPU %4.2f%%"%(100*self.cpu),"")

    def do_network(self):
        self.search_ip()

    def monitor_thread(self):
        mon = 0
        monitors = [self.do_cpu, self.do_network ]
        self.running = True
        while self.running:
            monitors[mon]()
            time.sleep(5)
            mon = (mon + 1) % len(monitors)

    def run(self):
        self.acquireLCD()
        monthread = threading.Thread(target=self.monitor_thread)
        monthread.daemon = True
        monthread.start()
        while True:
            b = self.buttons.wait_for_click()
            if b == self.buttons.cancel():
                self.running = False
                break
        monthread.join()
        self.releaseLCD()

if __name__ == "__main__":
    Monitors().run()

