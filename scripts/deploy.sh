#!/bin/sh

MAJOR_VERSION=0
MINOR_VERSION=8
PATCH_VERSION=0
EXTRA_VERSION='beta'

VERSION=$MAJOR_VERSION.$MINOR_VERSION.$PATCH_VERSION
[ -z "$EXTRA_VERSION" ] || VERSION=$VERSION-$EXTRA_VERSION

quiet() {
    "$@" >/dev/null 2>&1
}

binaries=("HardwareService.py" "TravelBox.py")

quiet mkdir .build
quiet mkdir -p .build/opt/TravelBox/bin
quiet mkdir -p .build/etc

cp -fav applications/* .build/opt/TravelBox/bin
cp -fav lib/* .build/opt/TravelBox/bin
cp -fav services/* deploy/start.sh .build/opt/TravelBox/bin
cp -fvr config/* .build/etc
cp -fav deploy/usr .build

tar czvf TravelBox-${VERSION}.tar.gz -C .build `ls .build`

