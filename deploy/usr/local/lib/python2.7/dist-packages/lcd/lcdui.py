#!/usr/bin/env python

class ProgressBar:
    def __init__(self, lcd, message, min=0, max=100, percent=False):
        self.lcd = lcd
        self.message = message
        self.minimum = min
        self.maximum = max
        self.count = max - min
        self.progress = min
        self.started = False
        self.shownum = percent
        if percent:
            self.delta = self.count / (self.lcd.rows - 6)
        else:
            self.delta = self.count / self.lcd.rows

    def start(self):
        self.started = True
        self.progress = self.minimum
        self.__draw__()
    
    def __draw__(self):
        value = 100 * self.progress / self.count
        chars = int(self.progress / self.delta)
        if self.shownum:
            spaces = (self.lcd.rows - 5) - chars
        else:
            spaces = self.lcd.rows - chars
        if value < 10: spaces += 1
        if value < 100: spaces += 1
        append = (" "*spaces) + str(value) + "%"
        self.lcd.write_line(self.message, 0)
        self.lcd.write_line(chr(255) * chars + append, 1)
    
    def step(self, amount=1):
        if not self.started:
            self.start()
        self.progress += amount
        self.__draw__()
        if self.progress >= self.maximum:
            self.started = False

    def setMessage(self, string):
        self.message = string
        if self.started:
            self.__draw__()
