#!/usr/bin/env python

import time

import Application

class LCDTest(Application.Application):
    def __init__(self):
        Application.Application.__init__(self,"LCD Hardware Test")
    
    def run(self):
        self.acquireLCD()
        self.write("Hello!", "It seem to work!")
        self.releaseLCD()

LCDTest().run()
