#!/usr/bin/env python

TRAVELBOX_IPC_KEY = b'travelbox'

import pickle
# change to syslog
import traceback

from multiprocessing.connection import Listener
import threading

class server:
    def __init__(self, port=25000, key=TRAVELBOX_IPC_KEY):
        self.serv = Listener(('localhost',port),authkey=key)
        self.delegates = {}

    def registerObject(self, name, obj):
        self.delegates[name] = obj

    def start(self):
        while True:
            try:
                client = self.serv.accept()
                thread = threading.Thread(target=self.run_service,args=(client,))
                thread.daemon = True
                thread.start()
            except EOFError:
                # client disconnected
                print "Client disconnected."
            except Exception:
                #change to syslog
                traceback.print_exc()

    def run_service(self, client):
        while True:         
            try:
                obj_name, name, args, kwargs = pickle.loads(client.recv())
                method = getattr(self.delegates[obj_name], name)
                result = pickle.dumps(method(*args,**kwargs))
                client.send(result)
            except EOFError:
                # client disconnected
                break
            except Exception as e:
                ex = pickle.dumps(e)
                client.send(ex)

from multiprocessing.connection import Client

class proxy:
    def __init__(self, obj_name, port=25000, key=TRAVELBOX_IPC_KEY):
        self.client = Client(('localhost',port), authkey=key)
        self.obj_name = obj_name

    def __getattr__(self, method):
        def do_rpc(*args, **kwargs):
            data = pickle.dumps((self.obj_name, method, args, kwargs))
            self.client.send(data)
            result = pickle.loads(self.client.recv())
            if isinstance(result, Exception):
                raise result
            return result
        return do_rpc


import sys

if __name__ == "__main__":
    class Hello:
        def __init__(self):
            pass
        def hello(self):
            return 'hello'

    
    class Another:
        def __init__(self):
            pass
        def another(self):
            return 'another'


    if sys.argv[1] == 'server':
        server = server()
        server.registerObject('travelbox.hello', Hello())
        server.registerObject('travelbox.another', Another())
        server.start()
    else:
        client = proxy('travelbox.hello')
        client2 = proxy('travelbox.hello')
        client3 = proxy('travelbox.hello')
        client4 = proxy('travelbox.hello')
        client5 = proxy('travelbox.another')
        print client.hello()
        print client2.hello()
        print client4.hello()
        print client4.hello()
        print client4.hello()
        print client4.hello()
        print client4.hello()
        print client4.hello()
        print client5.another()
