import i2c_lib
import time

# LCD Address
ADDRESS = 0x20

# commands
LCD_CLEARDISPLAY = 0x01
LCD_RETURNHOME = 0x02
LCD_ENTRYMODESET = 0x04
LCD_DISPLAYCONTROL = 0x08
LCD_CURSORSHIFT = 0x10
LCD_FUNCTIONSET = 0x20
LCD_SETCGRAMADDR = 0x40
LCD_SETDDRAMADDR = 0x80

# flags for display entry mode
LCD_ENTRYRIGHT = 0x00
LCD_ENTRYLEFT = 0x02
LCD_ENTRYSHIFTINCREMENT = 0x01
LCD_ENTRYSHIFTDECREMENT = 0x00

# flags for display on/off control
LCD_DISPLAYON = 0x04
LCD_DISPLAYOFF = 0x00
LCD_CURSORON = 0x02
LCD_CURSOROFF = 0x00
LCD_BLINKON = 0x01
LCD_BLINKOFF = 0x00

# flags for display/cursor shift
LCD_DISPLAYMOVE = 0x08
LCD_CURSORMOVE = 0x00
LCD_MOVERIGHT = 0x04
LCD_MOVELEFT = 0x00

# flags for function set
LCD_8BITMODE = 0x10
LCD_4BITMODE = 0x00
LCD_2LINE = 0x08
LCD_1LINE = 0x00
LCD_5x10DOTS = 0x04
LCD_5x8DOTS = 0x00

# flags for backlight control
LCD_BACKLIGHT = 0x08
LCD_NOBACKLIGHT = 0x00

En = 0b00000100 # Enable bit
Rw = 0b00000010 # Read/Write bit
Rs = 0b00000001 # Register select bit

class lcd:
    #initializes objects and lcd
    def __init__(self, lines = 2, rows = 16):

        self.lines = lines
        self.rows  = rows
        self.text  = [ "" for i in xrange(lines) ]

        self.lcd_device = i2c_lib.i2c_device(ADDRESS)

        self.write(0x03)
        self.write(0x03)
        self.write(0x03)
        self.write(0x02)

        if lines == 1: lcdlines = LCD_1LINE 
        else: lcdlines = LCD_2LINE 
        self.write(LCD_FUNCTIONSET | lcdlines | LCD_5x8DOTS | LCD_4BITMODE)
        self.write(LCD_DISPLAYCONTROL | LCD_DISPLAYON)
        self.write(LCD_CLEARDISPLAY)   
        self.write(LCD_ENTRYMODESET | LCD_ENTRYLEFT)
        time.sleep(0.2)   

    ## private
    # clocks EN to latch command
    def __strobe__(self, data):
        self.lcd_device.write_cmd(data | En | LCD_BACKLIGHT)
        time.sleep(.0005)
        self.lcd_device.write_cmd(((data & ~En) | LCD_BACKLIGHT))
        time.sleep(.0001)   

    ## private
    def __write_four_bits__(self, data):
        self.lcd_device.write_cmd(data | LCD_BACKLIGHT)
        self.__strobe__(data)

    # write a command to lcd
    def write(self, cmd, mode=0):
        self.__write_four_bits__(mode | (cmd & 0xF0))
        self.__write_four_bits__(mode | ((cmd << 4) & 0xF0))

    ## private
    # put string function
    def __display_text__(self):
        addresses = [0x80, 0xC0, 0x94, 0xD4]
        for line in xrange(self.lines):
            self.write(addresses[line])
            for char in self.text[line]:
                self.write(ord(char), Rs)
        #if line == 1:
        #    self.lcd_write(0x80)
        #if line == 2:
        #    self.lcd_write(0xC0)
        #if line == 3:
        #    self.lcd_write(0x94)
        #if line == 4:
        #    self.lcd_write(0xD4)
        #
        #for char in string:
        #    self.lcd_write(ord(char), Rs)

    def write_line(self, string, line):
        left = self.rows - len(string)
        self.text[line-1] = string + " "*left
        self.__display_text__()

    #def write_char(self, char):
    #    self.write(ord(char), Rs)

    # clear lcd and set to home
    def clear(self):
        self.write(LCD_CLEARDISPLAY)
        self.write(LCD_RETURNHOME)

